#include <stdio.h>

/*Timothy Bender
 * Lab 1 
 * CSCI 112 - Programming in C
 * 1/17/2020
*/

int main(void){
	double hours,minutes,time,temperature;
       	/*Declare variables. Doubles were decided to be easiest*/

	printf("Please enter hours: "); /*Get the hours*/
	scanf("%lf",&hours);

	printf("Please enter minutes: "); /*Get the minutes*/
	scanf("%lf",&minutes);

	time = hours + (minutes / 60); /*Determine the elapsed time*/

	temperature = (4*(time*time))/(time + 2) - 20; /*Perform the calculation*/

	printf("The temperature is %.2f degrees\n",temperature);
       	/*Print result, the double will be cut off at 2 decimal places*/
	
	return(0);
}
